<main class="container contenido">
    <form action="include/procesa.php" method="POST">
        <label for="nombre">Nombre del equipo:</label> <input type="text" name="nombre"><br /><br />

        <label for="categoria">Escoge una categoria:</label> 
        <select name="categoria">
            <option value="MotoGP">MotoGP</option>
            <option value="Moto2">Moto2</option>
            <option value="Moto3">Moto3</option>
        </select><br /><br />

        <label for="temporada">Escoge la temporada:</label> 
        <select name="temporada">
            <option value="2022">2022</option>
            <option value="2023">2023</option>
            <option value="2024">2024</option>
        </select><br /><br />

        <label for="piloto">Escoge el patrocinador principal:</label><br />
        <input type="radio" name="patrocinador" value="dainese"> Dainese<br />
        <input type="radio" name="patrocinador" value="alpinestars"> Alpinestars<br />
        <input type="radio" name="patrocinador" value="ixon"> Ixon<br /><br />

        <label for="mecanicos">¿Cuantos mecánicos quieres tener?</label> 
        <input type="number" name="mecanicos"><br /><br />

        <label for="piloto[]">Escoge los pilotos:</label><br />
        <input type="checkbox" name="piloto[]" value="marc"> Marc Márquez<br />
        <input type="checkbox" name="piloto[]" value="rossi"> Valentino Rossi<br />
        <input type="checkbox" name="piloto[]" value="jorge"> Jorge Martín<br /><br />

        <label for="puntuacion">Puntúa nuestra web:</label>
        <input type="number" name="puntuacion" max="5" min="0"><br /><br />

        <label for="comentario">¿Tienes alguna recomendación que darnos?</label><br />
        <textarea name="comentario" rows="4" cols="50"></textarea><br /><br />

        <input type="submit" value="¡Crear equipo!">
    </form>
</main>