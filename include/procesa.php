<?php
$nombre = filter_var($_POST['nombre'], FILTER_SANITIZE_STRING);
$categoria = filter_var($_POST['categoria'], FILTER_SANITIZE_STRING);
$temporada = intval($_POST['temporada']);
$patrocinador = filter_var($_POST['patrocinador'], FILTER_SANITIZE_STRING);
$mecanicos = intval($_POST['mecanicos']);
if(isset($_POST['piloto'])) {
    $pilotos = $_POST['piloto'];
}
$puntuacion = intval($_POST['puntuacion']);
if($puntuacion > 5) {
    $puntuacion = 5;
} else if($puntuacion < 0) {
    $puntuacion = 0;
}
$comentario = filter_var($_POST['comentario'], FILTER_SANITIZE_STRING);

include_once('cabecera.php');
?>


        <main class="container <?=$patrocinador?>">
            <section>
                <h1><?=$nombre?></h1>
            </section>
            <section>
                Competirá en la categoría: <strong><?=$categoria?></strong>
            </section>
            <section>
                Empezará a competir en la temporada: <strong><?=$temporada?></strong>
            </section>

            <section>
                Tu patrocinador principal es: (establece el background-color)
            <?php
            switch($patrocinador) {
                case 'dainese':
                    echo 'Dainese';
                    break;
                case 'alpinestars':
                    echo 'Alpinestars';
                    break;
                case 'ixon':
                    echo 'Ixon';
                    break;
                default:
                    echo 'Sin patrocinador';
                    break;
            }
            ?>
            </section>
            <section>
            Has contratatado <?=$mecanicos?> mecánicos.<br /><br />
            
            <?php
                echo str_repeat('<img src="../imagenes/mecanico.png" width="70px">', $mecanicos);
            ?>
            </section>
            <section>
                Has contratado <?=count($pilotos)?> pilotos, que son: <br /><br />
                <?php
                    if(isset($pilotos)) {
                        foreach($pilotos as $piloto) {
                            echo '<img src="../imagenes/'.$piloto.'.jpg" height="120px"> ';
                        }
                    } else {
                        echo 'No has elegido ningún piloto.';
                    }
                ?>
            </section>
            <section>
                Puntuación: <?=$puntuacion?><br />
                <span class="estrellas">
                <?php
                    echo str_repeat('★ ', $puntuacion);
                    echo str_repeat('☆ ', 5 - $puntuacion);
                ?>
                </span>
            </section>
            <section>
            Tu recomendación contiene <strong><?=str_word_count($comentario)-1?></strong> palabras.
            <br /><br />
            <?php
            foreach(explode(' ', $comentario) as $palabra) {
                if(strtoupper($palabra) == 'PHP') {
                    echo '<span class="palabra-php">'.$palabra.'</span> ';
                } else {
                    echo '<span class="palabra">'.$palabra.'</span> ';
                }
            }
            ?>
            </section>
        </main>
<?php
include_once('pie.php');
?>